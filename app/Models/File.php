<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = [
        'campaign_id', 'user_id', 'name'
    ];

    public function campaign()
    {
        return $this->hasOne(Campaign::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function spreadsheet()
    {
        return $this->hasMany(Spreadsheet::class);
    }
}
