<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Spreadsheet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
      'campaigns_id', 'file_id', 'item', 'status'
    ];

}
