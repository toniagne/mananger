<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SpreadsheetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct($params)
    {
        $this->details = $params;
    }

    public function build()
    {

        $template = explode('.html', $this->details['data']['HTML']);

        return $this->subject($this->details['data']['ASSUNTO'])
            ->view('emails.'.$template[0]);
    }
}
