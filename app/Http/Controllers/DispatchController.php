<?php

namespace App\Http\Controllers;

use App\Mail\SpreadsheetMail;
use App\Models\Campaign;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DispatchController extends Controller
{
    public function send(File $file)
    {
        foreach($file->spreadsheet()->get() as $item)
        {

            $spreadsheet_itens = $this->parse_params($item->item);

            foreach(json_decode($item->item) as $key => $params)
            {
                if ($key == "EMAIL"){
                    $details = [
                        'campaign_id' => $file->campaign_id,
                        'data' => $spreadsheet_itens
                    ];

                    Mail::to($params)
                        ->send(
                            new SpreadsheetMail($details)
                        );
                }
            }
        }
    }

    public function parse_params($params)
    {
        $result = [];

        foreach(json_decode($params) as $key => $params)
        {
            $key_parsed = str_replace("-", "_", $key);

            $result[$key_parsed] = $params;
        }

        return $result;
    }
}
