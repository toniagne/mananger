<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\File;
use App\Models\Spreadsheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpreadsheetController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        if ($request->hasFile('file'))
        {

            $file = $request->file('file');

            $file_spread = File::create([
                'campaign_id'   => $request->get('campaign_id'),
                'user_id'       => Auth::user()->id,
                'name'          => $file->getClientOriginalName(),
            ]);

            $csv = array_map('str_getcsv', file($file));
            array_walk($csv, function(&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });
            array_shift($csv); # remove column header

            foreach ($csv as $sheet)
            {
                $data = [
                    'campaigns_id'  => (int)$request->get('campaign_id'),
                    'file_id'       => $file_spread->id,
                    'item'          => json_encode($sheet),
                    'status'        => false
                ];

                Spreadsheet::create($data);
            }
        }

        return redirect(route('spreadsheets.show', $request->get('campaign_id')))->with(['success' => true]);
    }


    public function show($id)
    {
        $campaing = Campaign::find($id);

        return view('spreadsheet.show', compact('campaing'));
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
