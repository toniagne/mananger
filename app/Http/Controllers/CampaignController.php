<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;


class CampaignController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        return view('campaign.create');
    }


    public function store(Request $request)
    {
        $campaign = Campaign::create($request->all());

        return redirect(route('spreadsheets.show', $campaign->id));
    }


    public function show(Campaign $campaign)
    {
        return view('campaign.show', compact('campaign'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
