<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\SpreadsheetController;
use App\Http\Controllers\DispatchController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function () {

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('campaigns', CampaignController::class);

    Route::resource('spreadsheets', SpreadsheetController::class);

    Route::get('dispatch/{file}', [DispatchController::class, 'send'])->name('dispatch.send.file');
});

require __DIR__.'/auth.php';
