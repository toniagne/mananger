<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Eletropaulo</title>
</head>
<body>
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="display: block; border: none;">
    <tbody>
    <tr>
        <td height="217">
            <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_01']) }}" width="600" height="217" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="35" height="104">
                        <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_02']) }}" width="35" height="104" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                    <td width="529" height="104">
                        <table width="529" height="104" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px;">
                                    Nome / Razão Social:
                                </td>
                            </tr>
                            <tr>
                                <td width="529" height="81" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; font-family: Arial, Helvetica, sans-serif; color: #02405f; font-size: 12px; line-height: 12px; font-weight: bold;">
                                   {{ $details['data']['NOME'] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="36" height="104">
                        <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_04']) }}" width="36" height="104" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15">
            <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_05']) }}" width="600" height="15" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="35" height="93"><img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_06']) }}" width="35" height="93" alt="Eletropaulo" style="display: block; border: none;" border="0"></td>
                    <td width="529" height="93">
                        <table width="529" height="93" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px;">
                                    Endereço:
                                </td>
                            </tr>
                            <tr>
                                <td width="529" height="70" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; font-family: Arial, Helvetica, sans-serif; color: #02405f; font-size: 12px; line-height: 12px; font-weight: bold;">
                                    {{ $details['data']['manutencao_programada_emkt_04'] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="36" height="93"><img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_08']) }}" width="36" height="93" alt="Eletropaulo" style="display: block; border: none;" border="0"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="11">
            <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_09']) }}" width="600" height="11" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td>

            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="35" height="49">
                        <img src="{{ asset('images/'.$details['campaign_id'].'/'.$details['data']['manutencao_programada_emkt_10']) }}" width="35" height="49" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                    <td width="529" height="49">
                        <table width="100%" height="49" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr bgcolor="#f15c22">
                                <td width="138" height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    Nº de Instalação:
                                </td>
                                <td width="155" height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    Nº de Equipamento:
                                </td>
                                <td width="94" height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    Circuito:
                                </td>
                                <td width="142" height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    Data de Emissão:
                                </td>
                            </tr>
                            <tr bgcolor="#f2f3f4">
                                <td width="138" height="26" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    {{ $details['data']['nroinstalacao'] }}
                                </td>
                                <td width="155" height="26" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    {{ $details['data']['nroequipamento'] }}
                                </td>
                                <td width="94" height="26" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    {{ $details['data']['circuito'] }}
                                </td>
                                <td width="142" height="26" valign="middle" bgcolor="#f2f3f4" style="padding: 0 0 0 10px; border: none; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    {{ $details['data']['data'] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="36" height="49">
                        <img src="cid:%%manutencao-programada_emkt_12%%" width="36" height="49" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15">
            <img src="cid:%%manutencao-programada_emkt_13%%" width="600" height="15" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="35" height="177">
                        <img src="cid:%%manutencao-programada_emkt_14%%" width="35" height="177" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                    <td width="529" height="177">
                        <table width="529" height="177" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="23" valign="middle" bgcolor="#f15c22" style="padding: 0 0 0 10px; border: none; color: #ffffff; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">Desligamento (s) programado (s) para essa instalação:</td>
                            </tr>
                            <tr>
                                <td width="529" height="154" valign="top" bgcolor="#f2f3f4" style="padding: 5px 0 0 10px; border: none; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 12px; ">
                                    {{ $details['data']['interrupcao_1'] }}<br>
                                    {{ $details['data']['interrupcao_2'] }}<br>
                                    {{ $details['data']['interrupcao_3'] }}<br>
                                    {{ $details['data']['interrupcao_4'] }}<br>
                                    {{ $details['data']['interrupcao_5'] }}<br>
                                    {{ $details['data']['interrupcao_6'] }}
                                </td>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="36" height="177">
                        <img src="cid:%%manutencao-programada_emkt_16%%" width="36" height="177" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="27">
            <img src="cid:%%manutencao-programada_emkt_17%%" width="600" height="27" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td height="476">
            <img src="cid:%%manutencao-programada_emkt_18%%" width="600" height="476" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td height="467">
            <img src="cid:%%manutencao-programada_emkt_19%%" width="600" height="467" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td height="83">
            <img src="cid:%%manutencao-programada_emkt_20%%" width="600" height="83" alt="Eletropaulo" style="display: block; border: none;" border="0">
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td height="69">
                        <a href="http://www.eneldistribuicaosp.com.br/" target="_blank">
                            <img src="cid:%%manutencao-programada_emkt_21%%" width="210" height="69" alt="Eletropaulo" style="display: block; border: none;" border="0">
                        </a>
                    </td>
                    <td>
                        <img src="cid:%%manutencao-programada_emkt_22%%" width="119" height="69" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                    <td>
                        <a href="https://www.facebook.com/EnelBrasil/" target="_blank">
                            <img src="cid:%%manutencao-programada_emkt_23%%" width="102" height="69" alt="Eletropaulo" style="display: block; border: none;" border="0">
                        </a>
                    </td>
                    <td>
                        <img src="cid:%%manutencao-programada_emkt_24%%" width="169" height="69" alt="Eletropaulo" style="display: block; border: none;" border="0">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="100" style="display: block; border: none; color: #000000; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px; padding-top: 40px;">
            Este comunicado foi enviado para você, pois seu e-mail está cadastrado na nossa base.<br>Como a Enel é contra o spam na rede, caso você não deseje mais receber este, <a target="_blank" href="%%url%%%%chave_doc%%" >clique aqui</a>.
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
