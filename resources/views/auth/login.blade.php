<x-guest-layout>



    <div class="authincation h-100">
        <div class="container h-100">


            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">

                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Acesso restrito</h4>
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="mb-1"><strong>E-mail</strong></label>
                                            <input type="email" name='email' class="form-control" value="">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Senha</strong></label>
                                            <input type="password" name="password" class="form-control" value="">
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">

                                            <div class="form-group">
                                                <a href="{{ route('password.request') }}">Esqueceu a senha ?</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                                        </div>

                                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</x-guest-layout>
