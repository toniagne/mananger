<x-guest-layout>



        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">
                                        <h4 class="text-center mb-4">Cadastre-se</h4>
                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Nome</strong></label>
                                                <input type="text" name="name" class="form-control" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>E-mail</strong></label>
                                                <input type="email" name="email" class="form-control" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Senha</strong></label>
                                                <input type="password" name="password" class="form-control" value="">
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Confirme Senha</strong></label>
                                                <input type="password" name="password_confirmation" class="form-control" value="">
                                            </div>
                                            <div class="text-center mt-4">
                                                <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



</x-guest-layout>
