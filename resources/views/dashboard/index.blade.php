<x-app-layout>
    <x-header>
        <x-slot name="page_title">Dashboard</x-slot>
        <x-slot name="page_bradcrumb">Seja Bem vindo</x-slot>
    </x-header>

    <x-profile-slidebar></x-profile-slidebar>

    <div class="content-body">
        <div class="container-fluid">

        <div class="row">
            <div class="col-lg-8 col-xxl-12">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="card widget-stat ">
                            <div class="card-body p-4">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no">Total Planilhas</p>
                                        <h1 class="fs-36 font-w600 text-black mb-0">3</h1>
                                    </div>
                                    <span class="ml-3 bg-primary text-white">
												<i class="flaticon-381-promotion"></i>
											</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="card widget-stat">
                            <div class="card-body p-4">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no">Total Envios</p>
                                        <h1 class="fs-36 font-w600 d-flex align-items-center text-black mb-0">3</h1>
                                    </div>
                                    <span class="ml-3 bg-warning text-white">
												<i class="flaticon-381-user-7"></i>
											</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body d-flex flex-wrap p-0">
                                <div class="col-lg-6 col-sm-6  media spending-bx">
                                    <div class="spending-icon mr-4">
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        <span class="fs-14">+5%</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="fs-18 mb-2">Campanhas</p>
                                        <span class="fs-34 font-w600">23</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 media spending-bx pl-2">
                                    <div class="media-body text-right">
                                        <p class="fs-18 mb-2">Envios</p>
                                        <span class="fs-34 font-w600">3</span>
                                    </div>
                                    <div class="spending-icon ml-4">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                        <span class="fs-14">-2%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    </div>
</x-app-layout>
