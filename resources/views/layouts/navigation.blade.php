<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-networking"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="#">Estatisticas</a></li>
                    <li><a href="#">Campanhas</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-television"></i>
                    <span class="nav-text">Campanhas</span>
                </a>
                <ul aria-expanded="false">
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Campanhas</a>
                        <ul aria-expanded="false">
                            <li><a href="#">Nova</a></li>
                            <li><a href="#">Enviar</a></li>
                            <li><a href="#">Visualizar</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-controls-3"></i>
                    <span class="nav-text">Configuracoes</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="./chart-flot.html">Configuracoes e-mail</a></li>
                </ul>
            </li>

        </ul>
        <div class="copyright">
            <p><strong>FingerPrint</strong> | Mananger<br/>© {{ now()->format('Y')}}</p>
        </div>
    </div>
</div>
