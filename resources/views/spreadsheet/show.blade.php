<x-app-layout>
    <x-header>
        <x-slot name="page_title">Enviar CSV -> {{ $campaing->name }}</x-slot>
        <x-slot name="page_bradcrumb">
            <div>
                <a href="javascript:void(0);" class="fs-18 text-primary font-w600">Campanha / </a>
                <a href="{{ route('campaigns.show', $campaing->id) }}" class="fs-18 text-primary font-w600">{{ $campaing->name }} /</a>
                <a href="javascript:void(0);" class="fs-18">Enviar CSV</a>
            </div>
        </x-slot>
    </x-header>

    <x-profile-slidebar></x-profile-slidebar>

    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-xxl-12 col-lg-12">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header pb-0 border-0">
                                    <h3 class="fs-20 text-black">Envie o arquivo CSV</h3>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="{{ route('spreadsheets.store') }}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="campaign_id" value="{{ $campaing->id }}">
                                        <div class="form-row">
                                            <div class="form-group col-xl-12">
                                                <label class="text-black font-w500 mb-3">Planilha (CSV)</label><span class="text-danger ml-1">*</span>
                                                <input type="file" name="file" class="form-control">
                                            </div>
                                        </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">


                            <div class="form-row">
                                <div class="form-group col-xl-12">
                                    <x-button>Cadastrar</x-button>
                                </div>
                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Arquivos da campanha ({{$campaing->files()->count()}})</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-md">
                                    <thead>
                                    <tr>
                                        <th><strong>NOME</strong></th>
                                        <th><strong>USUARIO</strong></th>
                                        <th><strong>DATE</strong></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($campaing->files()->get() as $file)
                                    <tr>
                                        <td>{{ $file->name }}</td>
                                        <td>{{ $file->user->name }}</td>
                                        <td>{{ $file->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>