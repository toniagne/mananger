<x-app-layout>
    <x-header>
        <x-slot name="page_title">Nova campanha</x-slot>
        <x-slot name="page_bradcrumb">
            <div>
                <a href="javascript:void(0);" class="fs-18 text-primary font-w600">Campanha / </a>
                <a href="javascript:void(0);" class="fs-18">Criar nova campanha</a>
            </div>
        </x-slot>
    </x-header>

    <x-profile-slidebar></x-profile-slidebar>

    <div class="content-body">
        <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12 col-lg-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header pb-0 border-0">
                                <h3 class="fs-20 text-black">Descreva a campanha</h3>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('campaigns.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-xl-12">
                                            <label class="text-black font-w500 mb-3">Nome/Apelido</label><span class="text-danger ml-1">*</span>
                                            <input type="text" name="name" class="form-control" placeholder="">
                                        </div>

                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">


                        <div class="form-row">
                            <div class="form-group col-xl-12">
                                <x-button>Cadastrar</x-button>
                            </div>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>


</x-app-layout>