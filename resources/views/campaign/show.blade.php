<x-app-layout>
    <x-header>
        <x-slot name="page_title">Campanha -> {{ $campaign->name }}</x-slot>
        <x-slot name="page_bradcrumb">
            <div>
                <a href="javascript:void(0);" class="fs-18 text-primary font-w600">Campanha / </a>
                <a href="{{ route('campaigns.show', $campaign->id) }}" class="fs-18 text-primary font-w600">{{ $campaign->name }} /</a>
                <a href="javascript:void(0);" class="fs-18">Status da campanha</a>
            </div>
        </x-slot>
    </x-header>

    <x-profile-slidebar></x-profile-slidebar>

    <div class="content-body">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Arquivos da campanha ({{$campaign->files()->count()}})</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-md">
                                    <thead>
                                    <tr>
                                        <th><strong>NOME</strong></th>
                                        <th><strong>USUARIO</strong></th>
                                        <th><strong>DATE</strong></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($campaign->files()->get() as $file)
                                        <tr>
                                            <td>{{ $file->name }}</td>
                                            <td>{{ $file->user->name }}</td>
                                            <td>{{ $file->created_at->format('d/m/Y')}}</td>
                                            <td>
                                                @if(!$file->status)
                                                    <a href="{{ route('dispatch.send.file', $file->id) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-feed"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>