<div class="header">
    <div class="header-content">
        <div class="container-fluid">
            <div class="form-head d-flex mb-0 mb-lg-4 align-items-start">
                <div class="mr-auto d-none d-lg-block">
                    <h2 class="text-black font-w600 mb-1"> {{ $page_title }}</h2>
                </div>
                <div class="d-none d-lg-flex align-items-center">
                    <div class="text-right">
                        <a class="btn btn-primary" href="{{ route('campaigns.create') }}">+ Nova campanha</a>
                    </div>
                </div>
            </div>
            {{ $page_bradcrumb ?: '' }}
        </div>

    </div>
</div>